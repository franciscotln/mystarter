import { LoginPage } from './../login/login';
import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { App, Platform } from 'ionic-angular';

import { UsersService } from './../../app/core/services/users.service';
import { User } from './../../app/core/interfaces/user';

@Component({
  selector: 'page-others',
  templateUrl: 'others.html'
})
export class OthersPage {

  constructor(
    private appCtrl: App,
    private plt: Platform,
    private usersServ: UsersService,
    private storage: Storage
  ) { }

  public logout() {
    this.usersServ.currentUser = {} as User;
    ['jwt', 'isAccountant'].forEach(k => this.storage.remove(k));
    this.appCtrl.getRootNav().push(LoginPage);
  }
}
