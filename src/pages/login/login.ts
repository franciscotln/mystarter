import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { NavController, LoadingController, ViewController, AlertController } from 'ionic-angular';

import 'rxjs/add/operator/map';

import { UsersService } from './../../app/core/services/users.service';
import { TabsPage } from './../tabs/tabs';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;

  constructor(
    private navCtrl: NavController,
    private viewCtrl: ViewController,
    private fb: FormBuilder,
    private usersServ: UsersService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) { }

  public ngOnInit() {
    this.loginForm = this.fb.group({
      phoneNumber: ['', [ Validators.required, Validators.minLength(9), Validators.maxLength(9) ]],
      password: ['', Validators.required],
    });
  }

  ionViewWillEnter() {
    this.viewCtrl.showBackButton(false);
  }

  handleLogin(userData): void {
    if (this.loginForm.valid) {
      const spinner = this.loadingCtrl.create({ spinner: 'bubbles', content: 'Loading...' });
      spinner.present();

      this.usersServ.login(userData).subscribe(() => {
        this.navCtrl.push(TabsPage);
        spinner.dismiss();
      }, err => {
        spinner.dismiss();

        const subTitle = err.json().error;

        this.alertCtrl
          .create({ title: 'Error', subTitle, buttons: ['OK'] })
          .present();
      });
    }
  }
}
