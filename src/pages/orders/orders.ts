import { Component } from '@angular/core';
import { NavController, Refresher, InfiniteScroll } from 'ionic-angular';

import { OrderPage } from './../order/order';
import { OrdersService } from './../../app/core/services/orders.service';

@Component({
  selector: 'page-orders',
  templateUrl: 'orders.html'
})
export class OrdersPage {
  public orders = [];
  private resetParams = { resetOrders: true, resetOffset: true };

  constructor(
    private navCtrl: NavController,
    private ordersServ: OrdersService
  ) { }

  public ionViewWillEnter() {
    this.fetchOrders(this.resetParams);
  }

  public doRefresh(refresher: Refresher): void {
    this.fetchOrders(this.resetParams, refresher);
  }

  public doInfinite(infiniteScroll: InfiniteScroll): void {
    this.fetchOrders({}, infiniteScroll);
  }

  public openItem(orderNumber: string) {
    this.navCtrl.push(OrderPage, { orderNumber });
  }

  public setIconName({ crash_accident = 'none' }): string {
    const icons = {
      awaria: 'construct',
      wypadek: 'warning',
      none: ''
    };
    return icons[crash_accident];
  }

  private fetchOrders(resetParams, event?: InfiniteScroll | Refresher): void {
    const onComplete = !!event ? event.complete.bind(event) : function(){};
    this.ordersServ
      .fetchOrders(resetParams)
      .first()
      .subscribe(orders => this.orders = orders, null, onComplete);
  }
}
