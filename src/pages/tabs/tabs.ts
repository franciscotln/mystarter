import { Component } from '@angular/core';

import { OrdersPage } from '../orders/orders';
import { OthersPage } from '../others/others';
import { DriversPage } from '../drivers/drivers';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = OrdersPage;
  tab2Root: any = DriversPage;
  tab3Root: any = OthersPage;

  constructor() { }
}
