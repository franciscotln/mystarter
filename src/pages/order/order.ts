import { Component } from '@angular/core';
import { NavParams, AlertController, Alert } from 'ionic-angular';

import { UsersService } from './../../app/core/services/users.service';
import { OrdersService } from './../../app/core/services/orders.service';
import { STATUSES } from './../../app/core/constants/global.constants';

@Component({
  selector: 'page-order',
  templateUrl: 'order.html'
})
export class OrderPage {
  public order: any = {};
  public navbarColor: string = '';
  public navbarText: string = '';

  private showAlert = (alertConfig): Alert => {
    const alert = this.alertCtrl.create(alertConfig);
    alert.present();
    return alert;
  }

  private handleEmptyOrder = (order): void => {
    if (Object.keys(order).length === 0) {
      this.showAlert({
        title: 'No Order Number',
        subTitle: 'Cannot fetch from the server without the order number',
        buttons: ['OK']
      });
    }
  }

  constructor(
    private ordersServ: OrdersService,
    private params: NavParams,
    private alertCtrl: AlertController,
    private usersServ: UsersService
  ) { }

  public ionViewWillEnter() {
    this.ordersServ
      .fetchOrder(this.params.get('orderNumber'))
      .do(this.handleEmptyOrder)
      .first()
      .subscribe(order => {
        this.order = order;
        this.order.status = this.order.status || { id: 1, name: 'Awaiting' };
        this.navbarText = `${this.order.number} - ${this.order.status.name}`;
        this.navbarColor = STATUSES[this.order.status.id];
      });
  }

  public get isDispositor(): boolean {
    const isAssignedUser = !!this.order.assigned_user;
    const usersAreDifferent = (this.order.assigned_user || {}).id !== this.usersServ.currentUser.id;
    return isAssignedUser && usersAreDifferent;
  }
}
