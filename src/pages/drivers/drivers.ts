import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Observable } from 'rxjs/';

import { User } from './../../app/core/interfaces/user';
import { UsersService } from './../../app/core/services/users.service';

@Component({
  selector: 'page-drivers',
  templateUrl: 'drivers.html'
})
export class DriversPage {
  public drivers: Observable<User[]>;

  constructor(
    private navCtrl: NavController,
    private usersServ: UsersService
  ) { }

  public ionViewWillEnter() {
    this.drivers = this.usersServ.fetchUsers().first();
  }
}
