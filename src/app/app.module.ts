import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { IonicStorageModule, Storage} from '@ionic/storage';

import { MyApp } from './app.component';
import { OthersPage } from './../pages/others/others';
import { DriversPage } from './../pages/drivers/drivers';
import { OrdersPage } from '../pages/orders/orders';
import { OrderPage } from './../pages/order/order';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from './../pages/login/login';
import { getAuthHttp } from './core/services/get-auth-http';
import { UsersService } from './core/services/users.service';
import { OrdersService } from './core/services/orders.service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Http } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';

@NgModule({
  declarations: [
    MyApp,
    OthersPage,
    DriversPage,
    OrderPage,
    OrdersPage,
    TabsPage,
    LoginPage
  ],
  imports: [
    IonicModule.forRoot(MyApp, { iconMode: 'md' }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    OthersPage,
    DriversPage,
    OrdersPage,
    OrderPage,
    TabsPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    UsersService,
    OrdersService,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: AuthHttp, useFactory: getAuthHttp, deps: [Http, Storage] }
  ]
})
export class AppModule {}
