export interface User {
  id: number;
  is_accountant: boolean;
  phone_number: string;
  name: string;
  type: string;
}
