import { JwtHelper } from 'angular2-jwt';
import { Response } from '@angular/http';

const jwtHelper = new JwtHelper();

export function decodeToken(token: string) {
  return jwtHelper.decodeToken(token);
}

export function toJson <T> (res: Response): T {
  return res.json() as T;
}

export const API_URL: string = 'https://api-starter24-workspace.briiskdev.co/api/v1';

// status id -> css class name
export const STATUSES = {
  '1': 's-awaiting',
  '2': 's-accepted',
  '3': 's-in-progress',
  '4': 's-on-the-way',
  '5': 's-on-place',
  '6': 's-towing',
  '7': 's-finished',
  '8': 's-rejected',
  '9': 's-pre-finished',
  '10': 's-cancelled',
  '11': 's-order-details',
  '12': 's-approved',
  '13': 's-declined'
};
