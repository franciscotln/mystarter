import { Injectable } from '@angular/core';
import { AuthHttp, tokenNotExpired } from 'angular2-jwt';
import { Storage } from '@ionic/storage';

import { Observable } from 'rxjs/';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { User } from './../interfaces/user';
import { API_URL, decodeToken, toJson } from './../constants/global.constants';

@Injectable()
export class UsersService {
  public currentUser = {} as User;

  private storeJwt = ({ token }: { token: string }): Observable<string> => {
    return Observable.fromPromise(this.storage.set('jwt', token));
  };

  private fetchCurrentUser = (decodedUser: any): Observable<User> => {
    return this.http
      .get(`${API_URL}/users/${decodedUser.phone_number}`)
      .map(toJson);
  };

  private setCurrentUser = (user: User): User => {
    this.currentUser = user;
    return user;
  };

  private storeIsAccountant = (user: User): User => {
    this.storage.set('isAccountant', user.is_accountant);
    return user;
  };

  constructor(
    private http: AuthHttp,
    private storage: Storage
  ) { }

  public login({ phoneNumber, password }): Observable<User> {
    const data = { device_token: '', phone_number: phoneNumber, password };

    return this.http
      .post(`${API_URL}/users/sign_in`, data)
      .map(toJson)
      .switchMap(this.storeJwt)
      .map(decodeToken)
      .switchMap(this.fetchCurrentUser)
      .map(this.setCurrentUser)
      .map(this.storeIsAccountant);
  }

  public authentication$(): Observable<boolean> {
    return Observable
      .fromPromise(this.storage.get('jwt'))
      .map(token => tokenNotExpired(null, token));
  }

  public fetchUsers(): Observable<User[]> {
    return this.http
      .get(`${API_URL}/users`)
      .map(toJson);
  }
}
