import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';

import { Observable } from 'rxjs/';

import { API_URL, toJson } from './../constants/global.constants';

@Injectable()
export class OrdersService {
  private limit = 10;
  private offset = 0;
  private orders = [];

  private updateOffset = (): void => {
    this.offset += this.limit;
  }

  private handleOrders = (orders: any[] = []): any[] => {
    if (orders.length) {
      this.updateOffset();
      this.orders = this.orders.concat(orders);
    }
    return this.orders;
  }

  constructor(private http: AuthHttp) { }

  public fetchOrders({ resetOrders = false, resetOffset = false }): Observable<any[]> {
    if (resetOrders) {
      this.orders = [];
    }

    if (resetOffset) {
      this.offset = 0;
    }

    return this.http
      .get( `${API_URL}/orders?limit=${this.limit}&offset=${this.offset}`)
      .map(toJson)
      .map(this.handleOrders);
  }

  public fetchOrder(orderNumber: string): Observable<any> {
    if (!orderNumber) {
      return Observable.of({});
    }

    return this.http
      .get(`${API_URL}/orders/${orderNumber}`)
      .map(toJson);
  }
}
