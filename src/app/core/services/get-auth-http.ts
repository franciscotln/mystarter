import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { AuthHttp, AuthConfig } from 'angular2-jwt';

export function getAuthHttp(http: Http, storage: Storage) {
  const options = {
    noJwtError: true,
    globalHeaders: [
      { 'Accept': 'application/json' },
      { 'Content-Type': 'application/json' }
    ],
    tokenGetter() {
      return storage.get('jwt');
    },
  };

  return new AuthHttp(new AuthConfig(options), http);
}
