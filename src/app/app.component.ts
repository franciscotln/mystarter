import { Component, OnInit, OnDestroy } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { Subject } from 'rxjs/';

import { UsersService } from './core/services/users.service';
import { TabsPage } from './../pages/tabs/tabs';
import { LoginPage } from './../pages/login/login';

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit, OnDestroy {
  public rootPage: any;
  private killStreams = new Subject();

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private usersServ: UsersService
  ) { }

  ngOnInit(): void {
    this.setRootPage();
    this.handlePlatformReady();
  }

  ngOnDestroy(): void {
    this.killStreams.next(true);
  }

  private setRootPage(): void {
    this.usersServ
      .authentication$()
      .takeUntil(this.killStreams)
      .subscribe(isAuth => {
        this.rootPage = isAuth ? TabsPage : LoginPage;
      });
  }

  private handlePlatformReady(): void {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
